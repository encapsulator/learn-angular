import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  private baseUrl:String = "https://opentdb.com/api.php?";

  constructor(private http: HttpClient) {}

  getAll<T>(amount:number): Observable<T> {
    return this.http.get<T>(this.baseUrl+"amount="+amount);
  }

}
