export class Question {
  name:String;
  type:String;
  answers:String[];
}

export const QUESTIONS = [
  {name: 'Dr IQ', type:'multiple', answers:["A", "B", "C"]},
  {name: 'Magneta', type:'multiple', answers:["A", "B", "C"]},
  {name: 'Bombasto', type:'multiple', answers:["A", "B", "C"]}
];
